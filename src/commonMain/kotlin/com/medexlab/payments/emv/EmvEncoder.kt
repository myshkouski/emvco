package com.medexlab.payments.emv

import com.medexlab.payments.emv.extensions.percentEncoded
import com.medexlab.payments.emv.spec.IterablePayload
import com.medexlab.payments.emv.spec.Specification
import com.medexlab.payments.emv.spec.Tag

class EmvEncoder(
    private val specification: Specification,
) {
    fun encode(payload: IterablePayload): String {
        val crcTag = Tag(63)
        val map = payload.toMap().filterKeys {
            it != crcTag
        }
        val serializedPayload = PayloadImpl(map).serialize()
        val crc = crc(serializedPayload)
        return let {
            serializedPayload + PayloadImpl(mapOf(crcTag to crc)).serialize()
        }.percentEncoded()
    }

    companion object {
        fun crc(payload: String) = sha256sum(payload).takeLast(4).uppercase()
    }
}
