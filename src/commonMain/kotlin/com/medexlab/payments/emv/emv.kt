package com.medexlab.payments.emv

import com.medexlab.payments.emv.spec.Specification
import com.medexlab.payments.emv.spec.Tag
import com.medexlab.payments.emv.spec.defaultSpecification
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

private val SpecificationDefaults = object {
    val Version = object {
        val tag = Tag(0)
        val value = "01"
    }
}

private val defaultPayloadBuilder: PayloadScope.() -> Unit = {
    add(SpecificationDefaults.Version.tag, SpecificationDefaults.Version.value)
}

@OptIn(ExperimentalJsExport::class)
@JsExport
fun emv(
    specification: Specification = defaultSpecification(),
    builder: PayloadScope.() -> Unit
) = EmvEncoder(specification).encode(payload(defaultPayloadBuilder, builder))
