package com.medexlab.payments.emv

import com.medexlab.payments.emv.spec.ComparableTag
import com.medexlab.payments.emv.spec.IterablePayload
import com.medexlab.payments.emv.spec.Payload
import com.medexlab.payments.emv.spec.Tag
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

open class PayloadBuilder: PayloadScope {
    private val payload = mutableMapOf<ComparableTag, String>()

    override fun add(tag: Tag, value: String): PayloadBuilder {
        payload[Tag(tag.code)] = value
        return this
    }

    fun build(): IterablePayload = PayloadImpl(payload)
}

internal class PayloadImpl(
    private val map: Map<out Tag, String>
): IterablePayload {
    override fun get(tag: Tag): String? = map[tag]
    override fun iterator(): Iterator<Pair<Tag, String>> {
        return map.map {
            (key, value) -> key to value
        }.asIterable().iterator()
    }

    override fun serialize(): String {
        return iterator().asSequence().map { (code, value) ->
            val length = ("00" + value.length).takeLast(2)
            code to (length + value)
        }.sortedBy { (tag) -> tag.code }.map { (tag, value) ->
            "$tag$value"
        }.joinToString("")
    }
}

internal fun payload(vararg builders: PayloadScope.() -> Unit) = PayloadBuilder().apply {
    builders.forEach {
        apply(it)
    }
}.build()

@OptIn(ExperimentalJsExport::class)
@JsExport
fun payload(builder: PayloadScope.() -> Unit): Payload = payload(*arrayOf(builder))
