package com.medexlab.payments.emv.spec

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@OptIn(ExperimentalJsExport::class)
@JsExport
interface Payload {
    operator fun get(tag: Tag): String?
    fun serialize(): String
}

interface IterablePayload: Payload, Iterable<Pair<Tag, String>>
