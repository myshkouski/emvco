package com.medexlab.payments.emv.spec

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@OptIn(ExperimentalJsExport::class)
@JsExport
interface Field {
    val name: String
    val presence: Presence
}
