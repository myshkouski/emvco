package com.medexlab.payments.emv.spec

import com.medexlab.payments.emv.Resources
import com.medexlab.payments.emv.serialization.SpecReader
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@OptIn(ExperimentalJsExport::class)
@JsExport
interface Specification {
    val size: Int
    operator fun get(tag: String): Array<Field>
}

private class DefaultSpecification(
    private val value: Map<TagRange, Field>
) : Specification {
    override val size get() = value.size
    override fun get(tag: String) = get(Tag(tag))
    fun get(tag: Tag) = value.filterKeys {
        tag in it
    }.values.toTypedArray()
}

fun defaultSpecification(): Specification {
    val spec = SpecReader.read(Resources.spec)
    val map = spec.map { (key, value) ->
        val name by value
        val presence by value
        tagRange(key) to object : Field {
            override val name = name
            override val presence = Presence.byChar(presence.first())!!
        }
    }.toMap()

    return DefaultSpecification(map)
}
