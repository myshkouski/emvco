package com.medexlab.payments.emv.spec

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport
import kotlin.js.JsName
import kotlin.jvm.JvmStatic

@OptIn(ExperimentalJsExport::class)
@JsExport
interface Tag {
    @JsName("code")
    val code: Int
}

class ComparableTag(
    override val code: Int
): Tag, Comparable<Tag> {
    init {
        if(!allowedCodes.contains(code)) {
            throw Throwable("$code is out of range $allowedCodes")
        }
    }

    constructor(value: CharSequence): this(value.replace(Regex("^0+(?=\\d)"), "").toInt())

    override operator fun compareTo(other: Tag) = code.compareTo(other.code)

    override fun toString() = "00$code".takeLast(2)

    override fun equals(other: Any?): Boolean {
        return if (other is Tag) {
            compareTo(other) == 0
        } else super.equals(other)
    }

    override fun hashCode(): Int {
        return code
    }

    companion object {
        @JvmStatic
        val MIN_VALUE = 0
        @JvmStatic
        val MAX_VALUE = 99
        @JvmStatic
        private val allowedCodes = MIN_VALUE..MAX_VALUE
    }
}

fun Tag(value: Int) = ComparableTag(value)
fun Tag(value: CharSequence) = ComparableTag(value)

operator fun Tag.compareTo(other: Tag) = code.compareTo(other.code)
operator fun Tag.plus(value: Int): Tag = ComparableTag(this.code + value)
operator fun Tag.minus(value: Int) = plus(-1 * value)

private class TagIterator(
    start: Tag,
    private val endInclusive: Tag
) : Iterator<Tag> {
    private var current: Tag = start
    override fun hasNext() = current <= endInclusive
    override fun next() = current.also {
        current = it + 1
    }
}

class TagRange(
    start: Tag,
    endInclusive: Tag
): Iterable<Tag>, ClosedRange<ComparableTag> {
    override val start = Tag(start.code)
    override val endInclusive = Tag(endInclusive.code)

    override fun iterator(): Iterator<Tag> = TagIterator(start, endInclusive)

    override fun toString(): String {
        return if (start < endInclusive) {
            "$start-$endInclusive"
        } else {
            "$start"
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is TagRange) {
            start == other.start && endInclusive == other.endInclusive
        } else super.equals(other)
    }

    override fun hashCode(): Int {
        var result = start.hashCode()
        result = 31 * result + endInclusive.hashCode()
        return result
    }

    companion object {
        fun fromString(value: String) = Regex(
            "^(\\d{2})(-(\\d{2}))?\$"
        ).find(value)?.destructured?.let { (start, _, endInclusive) ->
            if (endInclusive.isEmpty()) {
                TagRange(Tag(start), Tag(start))
            } else {
                TagRange(Tag(start), Tag(endInclusive))
            }
        } ?: throw Throwable("invalid tag range format")
    }
}

operator fun Tag.rangeTo(to: Tag) = TagRange(this, to)
//operator fun Tag.rangeUntil(until: Tag) = TagRange(this, until - 1)

fun tagRange(value: String) = TagRange.fromString(value)
