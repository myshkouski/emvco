package com.medexlab.payments.emv.spec

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@OptIn(ExperimentalJsExport::class)
@JsExport
enum class Presence(
    val char: Char
) {
    Mandatory('m'),
    Optional('o'),
    Conditional('c');

    companion object {
        fun byChar(char: Char) = Presence.values().firstOrNull {
            it.char.lowercaseChar() == char.lowercaseChar()
        }
    }
}
