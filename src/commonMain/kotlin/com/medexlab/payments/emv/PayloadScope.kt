@file:OptIn(ExperimentalJsExport::class)

package com.medexlab.payments.emv

import com.medexlab.payments.emv.extensions.asTag
import com.medexlab.payments.emv.spec.Tag
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@JsExport
interface PayloadScope {
    fun add(tag: Tag, value: String): PayloadScope
}

fun PayloadScope.add(values: Map<out CharSequence, String>) {
    values.forEach { (key, value) ->
        add(
            key.asTag(),
            value
        )
    }
}

fun PayloadScope.add(vararg values: Pair<CharSequence, String>) = add(values.toMap())

@JsExport
fun PayloadScope.add(tag: Int, value: String) = add(Tag(tag) ,value)

//@JsExport
//fun PayloadScope.add(tag: String, value: String) = add(Tag(tag) ,value)
