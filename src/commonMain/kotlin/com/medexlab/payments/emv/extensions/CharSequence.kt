package com.medexlab.payments.emv.extensions

import com.medexlab.payments.emv.spec.Tag

fun CharSequence.asTag() = Tag(this)
