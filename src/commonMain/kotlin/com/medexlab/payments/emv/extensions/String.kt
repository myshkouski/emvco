package com.medexlab.payments.emv.extensions

expect fun String.percentEncoded(): String
