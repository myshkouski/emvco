package com.medexlab.payments.emv.serialization

typealias SpecMap = Map<String, Map<String, String>>

expect object SpecReader {
    fun read(name: String): SpecMap
}
