package com.medexlab.payments.emv.extensions

external fun encodeURIComponent(value: String): String

actual fun String.percentEncoded() = encodeURIComponent(this)
