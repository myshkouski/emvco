package com.medexlab.payments.emv.serialization

import com.medexlab.payments.emv.Resources
import com.medexlab.payments.emv.require
import kotlin.js.Json

private val JsObject = js("Object")
fun objectEntries(value: Json) = JsObject.entries(value) as Array<Array<dynamic>>
fun Json.toMap(): Map<String, dynamic> {
    if (!listOf("object", "array").contains(jsTypeOf(this))) {
        return mapOf()
    }
    return objectEntries(this).associate { (key, value) ->
        key.toString() to value
    }
}

actual object SpecReader {
    actual fun read(name: String): SpecMap {
        val jsonSpec = require(Resources.spec) as Json
        return jsonSpec.toMap().mapValues { (_, value) ->
            (value as Json).toMap()
        }
    }
}
