package com.medexlab.payments.emv

@JsModule("crypto-js/sha256")
@JsNonModule
private external fun jsSha256Sum(value: String): dynamic

actual fun sha256sum(value: String): String = jsSha256Sum(value).toString()
