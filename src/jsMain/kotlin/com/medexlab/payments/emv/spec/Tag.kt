package com.medexlab.payments.emv.spec

@OptIn(ExperimentalJsExport::class)
@JsExport
fun createTag(code: Int): Tag = ComparableTag(code)
