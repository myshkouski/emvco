package com.medexlab.payments.emv.spec

import com.medexlab.payments.emv.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class Tests {
    @Test
    fun parseSpec() {
        val spec = defaultSpecification()
        assertNotNull(spec)
        assertTrue { spec.size > 0 }
    }

    private val sourcePayload = mapOf(
        "00" to "01"
    )
    private val expectedEmvFragment = "000201" + "6304" + "BB41"

    private fun buildPayloadWithBuilder(): IterablePayload {
        val payloadBuilder = PayloadBuilder()
        payloadBuilder
            .add(Tag(0), "00")
            .add(Tag(1), "01")
        return payloadBuilder.build()
    }

    private fun buildPayloadWithExtensions(): Payload {
        val payload = payload {

        }

        return payload
    }

    fun buildEmvWIthEncoder(): String {
        val emvEncoder = EmvEncoder(defaultSpecification())
        val payload = buildPayloadWithBuilder()
        return emvEncoder.encode(payload)
    }

    @Test
    fun tags_are_unique_keys_in_map() {
        val map = mapOf(
            Tag(1) to "1",
            Tag(1) to "1"
        )
        assertEquals(1, map.size)
    }

    @Test
    fun build() {
        val actual = emv {
            add(sourcePayload)
        }
        assertEquals(expectedEmvFragment, actual)
    }

    @Test
    fun tagRange() {
        val range = Tag(2)..Tag(51)
        for (tag in range) {
            println(tag)
        }
    }
}
