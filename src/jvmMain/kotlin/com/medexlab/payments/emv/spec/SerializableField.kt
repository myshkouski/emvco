package com.medexlab.payments.emv.spec

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
internal data class SerializableField(
    @Contextual
    override val presence: Presence,
    override val name: String
): Field
