package com.medexlab.payments.emv.serialization

import com.medexlab.payments.emv.spec.Presence
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object PresenceSerializer: KSerializer<Presence> {
    override val descriptor = PrimitiveSerialDescriptor("Presence", PrimitiveKind.CHAR)

    override fun deserialize(decoder: Decoder): Presence {
        val char = decoder.decodeChar()
        return Presence.byChar(char) ?: throw NullPointerException("cannot find Presence with char '${char}'")
    }

    override fun serialize(encoder: Encoder, value: Presence) {
        encoder.encodeChar(value.char)
    }
}
