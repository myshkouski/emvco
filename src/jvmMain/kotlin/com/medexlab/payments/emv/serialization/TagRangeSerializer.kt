package com.medexlab.payments.emv.serialization

import com.medexlab.payments.emv.spec.TagRange
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object TagRangeSerializer : KSerializer<TagRange> {
    override val descriptor = PrimitiveSerialDescriptor(
        TagRange::class.simpleName!!,
        PrimitiveKind.STRING
    )

    override fun deserialize(decoder: Decoder): TagRange {
        val value = decoder.decodeString()
        return TagRange.fromString(value)
    }

    override fun serialize(encoder: Encoder, value: TagRange) {
        encoder.encodeString("$value")
    }
}
