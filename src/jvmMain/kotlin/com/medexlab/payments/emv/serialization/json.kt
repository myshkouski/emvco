package com.medexlab.payments.emv.serialization

import com.medexlab.payments.emv.spec.Field
import com.medexlab.payments.emv.spec.Presence
import com.medexlab.payments.emv.spec.SerializableField
import com.medexlab.payments.emv.spec.Tag
import com.medexlab.payments.emv.spec.TagRange
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.plus

val json = Json {
    serializersModule += SerializersModule {
        contextual(Tag::class) { TagSerializer }
        contextual(TagRange::class) { TagRangeSerializer }
        contextual(Presence::class) { PresenceSerializer }

        polymorphicDefault(Field::class) { SerializableField.serializer() }
    }
}
