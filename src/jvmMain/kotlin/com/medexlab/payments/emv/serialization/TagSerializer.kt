package com.medexlab.payments.emv.serialization

import com.medexlab.payments.emv.spec.Tag
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object TagSerializer : KSerializer<Tag> {
    override val descriptor = PrimitiveSerialDescriptor(
        Tag::class.simpleName!!,
        PrimitiveKind.STRING
    )

    override fun deserialize(decoder: Decoder): Tag {
        val value = decoder.decodeString()
        return Tag(value)
    }

    override fun serialize(encoder: Encoder, value: Tag) {
        encoder.encodeString("$value")
    }
}
