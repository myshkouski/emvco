package com.medexlab.payments.emv.serialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.decodeFromStream

actual object SpecReader {
    @OptIn(ExperimentalSerializationApi::class)
    actual fun read(name: String): SpecMap = javaClass.classLoader.getResource(name)!!.openStream().let {
        json.decodeFromStream(it)
    }
}
