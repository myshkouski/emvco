package com.medexlab.payments.emv.extensions

import java.net.URLEncoder

actual fun String.percentEncoded(): String = URLEncoder.encode(this, "utf-8")
