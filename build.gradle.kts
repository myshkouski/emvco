import org.jetbrains.kotlin.gradle.targets.js.npm.npmProject

plugins {
    val kotlinVersion = "1.8.10"
    kotlin("multiplatform") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
//    val npmPublishVersion = "2.1.2"
//    id("dev.petuska.npm.publish") version npmPublishVersion
}

group = "com.medexlab"
version = "0.0.4-alpha.2"

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        compilations.all {
//            kotlinOptions.jvmTarget = "1.8"
        }
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    js(IR) {
        nodejs {
            this@nodejs
        }
        useCommonJs()
        compilations.all {
            packageJson {
                // ./gradlew --no-daemon --console=plain -Pscope=orgName clean jsNpmPackage
                val scope: String? by project
                if (!scope.isNullOrEmpty()) {
                    name = "@$scope/" + this@all.npmProject.name
                }
            }
        }
        binaries.executable()
    }
    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting  {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
            }
        }
        val jvmTest by getting
        val jsMain by getting {
            dependencies {
                implementation(npm("crypto-js", "4.1.1"))
//                implementation(devNpm("webpack-bundle-analyzer", "4.7.0"))
//                implementation(devNpm("copy-webpack-plugin", "4.5.4"))
//                implementation(devNpm("webpack-node-externals", "3.0.0"))
//                implementation(devNpm("ignore-loader", "0.1.2"))
            }
        }
        val jsTest by getting
    }
}

val copyTestResourcesForJs by tasks.register<Copy>("jsCopyTestResources") {
    from(
        "$projectDir/src/commonMain/resources",
        "$projectDir/src/commonTest/resources"
    )
    into("${rootProject.buildDir}/js/packages/${rootProject.name}-test/kotlin")
}

tasks.named("jsNodeTest") {
    dependsOn(copyTestResourcesForJs)
}

val publicationsDir = "${rootProject.buildDir}/publications/npm/js"
val copyDist by tasks.register<Copy>("jsCopyDistDir") {
    from("${rootProject.buildDir}/js/packages/${rootProject.name}/")
    include("kotlin/**/*", "**/*.d.ts", "package.json")
    exclude("node_modules/")
    into(publicationsDir)
    dependsOn(tasks.named("compileProductionExecutableKotlinJs"))
}

val assemble by tasks.named("assemble")

val jsNpmPackage by tasks.register("jsNpmPackage") {
    dependsOn(assemble, copyDist)
}

//npmPublishing {
//    repositories {
//        repository("npmjs") {
//            registry = uri("https://registry.npmjs.org")
//            authToken = "<token>"
//        }
//    }
//    publications {
//        all {
//            files {
//                this
//            }
//        }
//    }
//}
